class JoystickController
{
  int horizontal_pin;
  int vertical_pin;
  int button_pin;
  public: 
    JoystickController(){}
    JoystickController(int horizontal_pin, int vertical_pin, int button_pin): horizontal_pin(horizontal_pin), vertical_pin(vertical_pin), button_pin(button_pin){}
    int GetVerticalValue()
    {
      //analogRead(vertical_pin);
      return map(analogRead(vertical_pin), 0, 1023, -1, 1);
    }
    int GetHorizontalValue()
    {
      //analogRead(horizontal_pin);
      return map(analogRead(horizontal_pin), 0, 1023, -1, 1);
    }
    bool LeftPushed()
    {
      return GetHorizontalValue() > 0;
    }
    bool RightPushed()
    {
      return GetHorizontalValue() < 0;
    }
    bool UpPushed()
    {
      return GetVerticalValue() > 0;
    }
    bool DownPushed()
    {
      return GetVerticalValue() < 0;
    }
    bool CentralPushed(){
      return !digitalRead(button_pin);
    }
};