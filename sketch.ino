#include "Gun.h"
#include "ButtonController.h"
#include <LiquidCrystal_I2C.h>
#include "JoystickController.h"
int buttons = true; // if true - control through buttons, else - through joystick
//Buttons
int left_b_pin = A1;
int right_b_pin = A2;
int up_b_pin = A3;
int down_b_pin = A0;
int shoot_b_pin = 13;
//Led
int led_pin = 2;
//Buzzer
int buzzer_pin = 12;
//Joystick
int j_h_pin = A6;
int j_v_pin = A7;
int j_b_pin = 3;

int right_left_servo_pin=1;
int up_down_servo_pin=0;

LiquidCrystal_I2C lcd(0x27, 20, 4);

Gun gun; 
ButtonController controller;
JoystickController joystick;

void setup()
{
  pinMode(left_b_pin, INPUT_PULLUP); 
  pinMode(right_b_pin, INPUT_PULLUP);
  pinMode(up_b_pin, INPUT_PULLUP);
  pinMode(down_b_pin, INPUT_PULLUP);
  pinMode(shoot_b_pin, INPUT_PULLUP);
  pinMode(led_pin, OUTPUT);
  pinMode(buzzer_pin, OUTPUT);
  pinMode(j_h_pin, INPUT);
  pinMode(j_v_pin, INPUT);
  pinMode(j_b_pin, INPUT_PULLUP);

  lcd.init();
  lcd.backlight();
  
  gun = Gun(right_left_servo_pin, up_down_servo_pin, led_pin, buzzer_pin);
  controller = ButtonController(right_b_pin, left_b_pin, up_b_pin, down_b_pin, shoot_b_pin);
  joystick = JoystickController(j_h_pin, j_v_pin, j_b_pin);
}

void loop()
{
  lcd.setCursor(3,0);
  if ( controller.LeftPushed() || joystick.LeftPushed())// Check for the yellow button input
  {
    lcd.print("              ");
    gun.MoveLeft();
  }
  if (controller.RightPushed() || joystick.RightPushed()) // Check for the blue button input
  {
    gun.MoveRight();
    lcd.print("              ");
  }
  if (controller.UpPushed() || joystick.UpPushed()) // Check for the yellow button input
  {
    gun.MoveUp();
    lcd.print("              ");
  }
  if (controller.DownPushed() || joystick.DownPushed()) // Check for the blue button input
  {
    gun.MoveDown();    
    lcd.print("              ");
  }
  if ( controller.CentralPushed() || joystick.CentralPushed()) // Check for the blue button input
  {
    gun.Shoot();
    lcd.print("PIU-PIU!");
  }
  lcd.setCursor(3, 2);
  lcd.print("Horizontal: ");
  lcd.print(gun.GetRLPos());
  lcd.setCursor(3, 3);
  lcd.print("Vertical: ");
  lcd.print(gun.GetUDPos());
}
