#include <Servo.h>
class Gun 
{
  float step = 1.0;
  float RL_pos = 0.0;
  float UD_pos = 0.0;
  int RL_servo_pin;
  int UD_servo_pin; 
  int LED_pin;
  int BUZZ_pin;
  Servo RL_servo;
  Servo UD_servo;
  public: 
    Gun (){}
    Gun(int RL_servo_pin, int UD_servo_pin, int led_pin, int buzzer_pin) : RL_servo_pin(RL_servo_pin), UD_servo_pin(UD_servo_pin), LED_pin(led_pin), BUZZ_pin(buzzer_pin) 
    {
      RL_servo.attach(RL_servo_pin); // Attache the arm to the pin
      UD_servo.attach(UD_servo_pin); 
      SetUp();
    }
    void SetUp()
    {
      RL_servo.write(0.0);
      UD_servo.write(0.0); // Initialize the arm's position to 0 (leftmost)
    }
    void MoveUp() {
      if (UD_pos > 0) // Check that the position won't go lower than 0°
      {
        UD_servo.write(UD_pos); // Set the arm's position to "pos" value
        UD_pos-=step; // Decrement "pos" of "step" value
        delay(5); // Wait 5ms for the arm to reach the position
      }
    }
    void MoveDown() {
      if (UD_pos < 180) // Check that the position won't go higher than 180°
      {
        UD_servo.write(UD_pos); // Set the arm's position to "pos" value
        UD_pos+=step; // Increment "pos" of "step" value
        delay(5); // Wait 5ms for the arm to reach the position
      }
    }
    void MoveLeft() {
      if (RL_pos > 0) // Check that the position won't go lower than 0°
      {
        RL_servo.write(RL_pos); // Set the arm's position to "pos" value
        RL_pos-=step; // Decrement "pos" of "step" value
        delay(5); // Wait 5ms for the arm to reach the position
      }
    }
    void MoveRight() {
      if (RL_pos <180) // Check that the position won't go higher than 180°
      {
        RL_servo.write(RL_pos); // Set the arm's position to "pos" value
        RL_pos+=step; // Increment "pos" of "step" value
        delay(5); // Wait 5ms for the arm to reach the position
      }
    }
    float GetRLPos() {
      return RL_pos;
    }
    float GetUDPos() {
      return UD_pos;
    }

    void Shoot () {
      tone(BUZZ_pin, 300, 5);
      digitalWrite(LED_pin, HIGH);
      delay(5);
      digitalWrite(LED_pin, LOW);
      delay(5);
    }
};