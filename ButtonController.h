class ButtonController 
{
  int R_button_pin;
  int L_button_pin; 
  int U_button_pin;
  int D_button_pin;
  int C_button_pin; //center button
  public: 
    ButtonController (){}
    ButtonController(int R_button_pin, int L_button_pin, int U_button_pin, int D_button_pin, int C_button_pin) : R_button_pin(R_button_pin), L_button_pin(L_button_pin), U_button_pin(U_button_pin), D_button_pin(D_button_pin), C_button_pin(C_button_pin){}
    bool UpPushed() {
      return !digitalRead(U_button_pin);
    }
    bool DownPushed() {
      return !digitalRead(D_button_pin);
    }
    bool LeftPushed() {
      return !digitalRead(L_button_pin);
    }
    bool RightPushed() {
      return !digitalRead(R_button_pin);
    }
    bool CentralPushed() {
      return !digitalRead(C_button_pin);
    }
};