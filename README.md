# Gun simulation

This project is an assignment in C++/Embedded course. This is a gun simulation. <br>
The assignment is done using online simulator found [here](https://wokwi.com/). <br> 


## Usage

* Go to [Wokwi.org](https://wokwi.com/)
* Scroll down to "Start from scratch" section. 
* Choose Arduino Nano.
* Copy the contents of files from the repository to the corresponding tabs on the left side of the window.
* Press play-button to see the simulation in action.

The servo motor on the left represents gun movement in the horizontal plane (right-left).<br>
The servo motor on the right represents gun movement in the vertical plane (up-down).<br>
The buzzer and the LED represent a shot.<br>
To operate the gun, button controls on the bottom can be used, as well as joystick on the top right. <br>
![scheme](scheme.png)

During simulation the lcd display will output gun position (0-180 degrees) 

### Maintainers
@MariaNema
